package com.example.darshit.myfirstapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by dell on 19/5/16.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "AppName";
    private static final int DB_VERSION = 1;

    DatabaseHelper(Context context){
        super(context,DB_NAME,null, DB_VERSION);
    }


    public void onCreate(SQLiteDatabase db){
        db.execSQL("CREATE TABLE MESSAGES ("
                    + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "DATE TEXT,"
                    + "SENDER INTEGER, "
                    + "CONVERSATION TEXT,"
                    + "MESSAGE TEXT );"
        );
    }


    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){

    }
}
