package com.example.darshit.myfirstapp;

import android.app.Activity;
import android.app.Fragment;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by dell on 17/5/16.
 */
public class ChatHistory extends Fragment
{
    /**ChatHistoryFragment is the fragment that displays to the user the history of the chat with
     *  the user specified during fragment creation.*/
    private static Activity activity;
    private static ListView listView;
    private static ArrayList<String> history = new ArrayList<String>();
    private static CursorAdapter history_adapter;
    private static Cursor cursor;
    private static SQLiteDatabase db;
    private static String current_conversation;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //Inflate the layout for this segment
        View rootview = inflater.inflate(R.layout.chat_history_fragment,container, false);


        //TODO: Get the current conversation value and activity here
        current_conversation = "SELF";
        activity = getActivity();

        //TODO: Initialize the history here
        new DatabaseFetchAndUpdateTask(false).execute(1);

        return rootview;
    }

    public void update()
    {
        /** ChatHistoryFragment.update() method serves to update the chat history fragment whenever the
         * user sends a message.It does so by calling the DatabaseFetchAndUpdateTask's execute method*/
        //TODO: Updating the list to show new history
        new DatabaseFetchAndUpdateTask(true).execute(1);
        //Making the cursor again because cursors fetch all data at the time of creation

    }

    public void onDestroy()
    {
        super.onDestroy();
        cursor.close();
        db.close();
    }

    private class DatabaseFetchAndUpdateTask extends AsyncTask<Integer, Void, Boolean>
    {
        /** This class is an inner class of ChatHistoryFragment.It extends AsyncTask and serves to
         * update the database and notify the cloud service of the new message. It also serves to
         * create the chat history the first time the fragment is created. It does so in the
         * background so that the UI does not become non-responsive when the history is loading.*/


        private EditText editText;
        private boolean is_update_call;
        String new_message;

        private DatabaseFetchAndUpdateTask(boolean is_update_call )
        {
            super();
            this.is_update_call = is_update_call;
        }

        protected void onPreExecute()
        {
            if (is_update_call)
            {
                //TODO: Parsing and fetching the editText data
                editText = (EditText) activity.findViewById(R.id.edit_message);
                new_message = editText.getText().toString();
                editText.setText("");
            }

        }

        protected Boolean doInBackground(Integer... integers)
        {
            //Code that you want to run in the background thread
            if (is_update_call)
            {
                //TODO: Fetch user's date and time/current date and time from the cloud, convert it to suitable format and add to the database
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                String date = simpleDateFormat.format(new Date());


                //TODO: Update the change in state of conversation in the Cloud and notify the other user of the same


                //TODO: Update the data value in user's database
                //Making the Contentvalues object and adding the message content to it to add to the database
                ContentValues message = new ContentValues();
                message.put("DATE",date);
                message.put("SENDER", 0);
                message.put("CONVERSATION","SELF");
                message.put("MESSAGE", new_message);

                //Adding the content to the database
                db.insert("MESSAGES",null,message);
            }
            else
            {
                try
                {
                    DatabaseHelper databaseHelper = new DatabaseHelper(activity);
                    db = databaseHelper.getWritableDatabase();

                    //TODO: Use the date, sender and conversation values obtained in display, i.e. different layouts for different senders

                }
                catch ( SQLiteException sqliteexception)
                {
                    Toast toast = Toast.makeText(activity, "Database unavailable", Toast.LENGTH_SHORT);
                    toast.show();

                }
            }


            Cursor cursor1 = db.query(
                    "MESSAGES",
                    new String[]{"_id","DATE","SENDER","CONVERSATION","MESSAGE"},
                    "CONVERSATION =  ?",
                    new String[]{current_conversation},
                    null,
                    null,
                    "DATE ASC");

            cursor = cursor1;
            if (is_update_call==false)
            {
                //TODO: adapter initialization and addition to listview
                history_adapter = new SimpleCursorAdapter(activity,R.layout.sent_message,cursor,new String[]{"MESSAGE"},new int[]{R.id.sent_message},0);
                listView = (ListView) activity.findViewById(R.id.history_list_view);
                Log.i("Events: updatecall",Integer.toString(cursor.getCount()));
            }

            return true;
        }

        protected void onProgressUpdate()
        {
            //Code that you want to run to publish the progress of your task
        }

        protected void onPostExecute(Boolean b)
        {
            //Code that you want to run when the task is complete

            if (is_update_call)
            {
                history_adapter.changeCursor(cursor);
                Log.i("Events: updatecall",Integer.toString(cursor.getCount()));
                editText.requestFocus();
            }
            else
            {
                //TODO: adapter initialization and addition to listview
                listView.setAdapter(history_adapter);
            }

        }
    }
}
